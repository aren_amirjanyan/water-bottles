exports.waterBottles = function (readBootles, blueBootles) {
    var readKey = 0;
    var blueKey = 0;
    var finalResponse = [];
    var response = [];
    var xor = 0;
    while (blueKey < blueBootles.length && readKey < readBootles.length) {

        var diff = blueBootles[blueKey] - readBootles[readKey];

        if (diff > 0) {
            xor = blueBootles[blueKey]-diff;
            blueBootles[blueKey] = diff;
            response.push('Read Bootle '+(readKey+1)+' => '+xor+' liter');
            readKey++;
        }
        else if (diff < 0) {
            xor = readBootles[readKey]+diff;
            readBootles[readKey]=-diff;
            response.push('Read Bootle '+(readKey+1)+' => '+xor+' liter');
            blueKey++;
            finalResponse.push(response);
            response = [];
        } else {
            xor = readBootles[readKey];
            response.push('Read Bootle '+(readKey+1)+' => '+xor+' liter');
            readKey++;
            blueKey++;
            finalResponse.push(response);
            response = [];
        }

    }

    return finalResponse;
}
